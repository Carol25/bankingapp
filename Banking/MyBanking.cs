using System;

namespace Banking
{
/// <summary>
/// dertermines the tax bracket of a given salary and deducts taxes and medical aids
/// </summary>

    public class MyBanking
    {
        public double salary;

        public double taxPercentage;

        public MyBanking(double s)
        {
            salary = s;

        }

///     <summary>
///     this method wil subtract the tax from the original salary using the tax percent
///     </summary>
///     <param name="salary"> A <see cref="double">double</see> which is the original salary </param>
///     <param name="taxPercent"> 
///     A <see cref="double">double</see> that will be used to calculate tax deducatble amount  
///     </param>

        public double TaxDeductions(double salary, double taxPercent)
        {
            double taxDeductable = salary * taxPercent / 100;
            double salaryAfterTax = salary - taxDeductable;
            return salaryAfterTax;
        }

///     <summary>dertermines the tax brackect based on the salary given</summary>
///     <param name="salary"> A <see cref="double"> double </see> used to calculate tax bracket</param>
///     <returns>its a  double that represents tax Percentage</returns>
        
        public double CalculateTaxBracket(double salary) 
        {
           Console.WriteLine("My salary is " + salary);
            if(salary <= 10000.00) 
            {
                return 10.00;
            }
            else
            {
                return 18.00;
            }
        }

///     <summary>
///     This method will subtract 5% medial aid deductions from salary
///     </summary>
///     <param name="salary"> A <see cref="double">double</see> from which 5% will be deducted </param>
///     <returns> The salary after medical aid deductions </returns>
        public double MedicalAidDeductions(double salary)
        {
            double deductable = salary * 5 / 100;
            double salaryAfterMedicalDeductions = salary - deductable;
            return salaryAfterMedicalDeductions;
        }
        
    }
}
