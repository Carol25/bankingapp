using System;
using Xunit;
using Banking;

namespace BankingTest
{
/// <summary>
/// used to test MyBanking Class
/// </summary>   
    public class BankingTest
    {
        
 ///    <summary>
 ///    this is to test if tax brackect is 10.00 given salary  below or equal to 10 000
 ///    </summary>      
    
        [Fact]
        public void CalculateTaxBracketLowerTest()
        {
            MyBanking myBank = new MyBanking(9800.00);

            double taxPercent = myBank.CalculateTaxBracket(myBank.salary);

            Assert.Equal(10.00 ,taxPercent);

        }
///     <summary>
///     this is to test if tax brackect is 18.00 given salary  above  to 10 000
///     </summary>       
        [Fact]
        public void CalculateTaxBracketHigherTest()
        {

          // Arrange
          MyBanking myBank = new MyBanking(12500.00);
          // Act
          double taxPercent  = myBank.CalculateTaxBracket(myBank.salary);
          //Assert
          Assert.Equal(18.00 , taxPercent);

        }

///     <summary>
///     this method is to test if the tax deductable is calculated and subtracted correctly
///     </summary>

        [Fact]
        public void TaxDeductionsTest()
        {
            // Arrange 
            MyBanking myBank = new MyBanking(9000);            
            double taxBracket = myBank.CalculateTaxBracket(myBank.salary);            
            double deductableAmount = myBank.salary * taxBracket / 100;
            double salaryAfterTax = myBank.salary - deductableAmount;

            // Act 
            double result = myBank.TaxDeductions(myBank.salary, taxBracket);

            // Assert           
            Assert.Equal(salaryAfterTax, result);
        }
        
///     <summary>
///     This will deduct 5% of salary for medical aid and return the balance
///     </summary>
///     <param name="salary"> A <see cref="double">double</see> from which 5% will be deducted </param>

        [Fact]        
        public void MedicalAidDeductionsTest()
        {
            // Arrange
            MyBanking myBank = new MyBanking(10000);
            double salaryAfterMedicalDeductions;            
            double expectedDeductions = myBank.salary * 5 / 100;
            double expectedSalaryAfterDeductions = myBank.salary - expectedDeductions;

            // Act 
            salaryAfterMedicalDeductions = myBank.MedicalAidDeductions(myBank.salary);

            // Assert           
            Assert.Equal(salaryAfterMedicalDeductions, expectedSalaryAfterDeductions);
        }
    }
}
